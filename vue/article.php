<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Affichage d'un article</title>
	<link rel="stylesheet" type="text/css" href="../assets/design/style.css">

	<link href="../assets/utilities/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

	<link href="../assets/utilities/css/sb-admin-2.min.css" rel="stylesheet">>

</head>
<body>
	<div id="contenu">
	<?php 
		require_once 'entete.php';

		
		if (isset($article))
		{
			?>
				
				<h1><?= $article->titre ?></h1>
				<span>Publié le <?= $article->dateCreation ?></span>
				<p><?= $article->contenu ?></p>
			</div><?php
		}
		else if  (isset($articles))
		{
			

			if (empty($articles)) {
				echo "Aucun article dans cette catégorie";
			}
			else
			{
				foreach ($articles as $article)
				{?>
					<div class="article">
						<h1><a href="article/<?= $article->id ?>"><?= $article->titre ?></a></h1>
						<p><?= substr($article->contenu, 0, 300) . '...' ?></p>
					</div><?php
				}
			}
		}
		else
		{		foreach ($articles as $article)
				{?>
					<div class="article">
						<h1><a href="article/<?= $article->id ?>"><?= $article->titre ?></a></h1>
						<p><?= substr($article->contenu, 0, 300) . '...' ?></p>
					</div><?php
				}

			}
		
		?>
	</div>
	

	<?php 
		require_once 'menu.php'; 
	?>
</div>

</body>
</html>