<div id="menu">


	<div>
	  <button class="btn btn-primary" onclick="window.location.href='login.php'" id="" style="margin-bottom: 100px;">
              <h6>Log In<i class="fas fa-sign-in-alt"></i></h6>
              
	  </button>
	</div>

	<h1>Catégories</h1><hr width="20%">
	<ul>
		<li><a href="home">Tout</a></li>
		<?php foreach ($categories as $categorie): ?>
			<li><a href="categorie/<?= $categorie->id ?>"><?= $categorie->libelle ?></a></li>
		<?php endforeach ?>
	</ul>

</div>