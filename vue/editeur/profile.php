<?php

require '../../dao/database.php';

session_start ();
if(!isset($_SESSION['user'])) header("Location: ../../login.php");
$name= "";
$nom= "";
$prenom= "";
$numEditeur= "";
$email= "";
$account= "";
$pwd= "";
$id= "";

if (isset($_SESSION['user']['profil'])) {
    $name= $_SESSION['user']['login'];
    $email= $_SESSION['user']['login'];
    $pwd=$_SESSION['user']['pwd'];
}
else {
    $name= $_SESSION['user']['prenom']." ".$_SESSION['user']['nom'];
    $nom= $_SESSION['user']['nom'];
    $prenom= $_SESSION['user']['prenom'];
    $numEditeur= $_SESSION['user']['numEditeur'];
    $email= $_SESSION['user']['email'];
    $id= $_SESSION['user']['id'];
    $account= Database::getOneAccount($email);
    $pwd= $account['pwd'];
}


if (isset($_POST['nom'])) {
    if (isset($_SESSION['user']['profil'])) {
       Database::createEditeur($_POST['numEditeur'], $_POST['nom'], $_POST['prenom'], $email);
       $_SESSION['user']= Database::checkEditeur($email,$pwd);
    }
    else {
        Database::updateEditeur($id, $_POST['numEditeur'], $_POST['nom'], $_POST['prenom'], $email);
        $_SESSION['user']= Database::checkEditeur($email,$pwd);
    }
}


?>





<!DOCTYPE html>
<html lang="en">

<head> 

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PT</title>

  <!-- Custom fonts for this template-->
  <link href="../../assets/utilities/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../assets/utilities/css/sb-admin-2.min.css" rel="stylesheet">

  <script type="text/javascript">
    function setFields(){
        document.getElementById("nom").placeholder = "Nom";
        document.getElementById("prenom").placeholder = "Prenom";
        document.getElementById("numEditeur").placeholder = "Num Editeur";

        document.getElementById("nom").value = "<?= $nom?>";
        document.getElementById("prenom").value = "<?= $prenom?>";
        document.getElementById("numEditeur").value = "<?= $numEditeur?>";

        document.getElementById("nom").readOnly = false;
        document.getElementById("prenom").readOnly = false;
        document.getElementById("numEditeur").readOnly = false;

        document.getElementById("editBtn").disabled = true;
        document.getElementById("submitBtn").disabled = false;
    }
  </script>

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Your profile</h1>
              </div>
              
              <form class="user" name="adminRegister" action="profile.php?updatedProfile=1" method="post">
                <button onclick="setFields();" id="editBtn" name="editBtn" class="btn btn-info btn-user">
                <i class="far fa-edit"></i> Edit
                </button>
                <hr>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="nom" id="nom" value="<?= $nom?>" placeholder="Nom" readonly required>
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-user" name="prenom" id="prenom" value="<?= $prenom?>" placeholder="Prenom" readonly required>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="numEditeur" id="numEditeur" value="<?= $numEditeur?>" placeholder="Num Editeur" readonly required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="email" id="email" value="<?= $email?>" placeholder="Email" readonly required>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control form-control-user" name="password" id="password" value="<?= $pwd?>" placeholder="Password" readonly required>
                </div>
                <input type="submit" name="submitBtn" id="submitBtn" class="btn btn-primary btn-user btn-block" value="Update Profile" disabled>
                <hr>
                <a href="." class="btn btn-google btn-user btn-block">
                <i class="fas fa-home"></i> Quitter
                </a>
              </form>
              <hr>
             
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- accountDeleteSuccessModal Modal-->
  <div class="modal fade" id="profileUpdateSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Profile Updating</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Your <strong>profile</strong> updated successfully !</div>
        <div class="modal-footer">
          <a class="btn btn-primary" href="profile.php">Done</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../../assets/utilities/vendor/jquery/jquery.min.js"></script>
  <script src="../../assets/utilities/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../assets/utilities/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../assets/utilities/js/sb-admin-2.min.js"></script>


  
  <?php
  if(isset($_GET['updatedProfile']) && $_GET['updatedProfile']==1){
        echo "<script type='text/javascript'>
                $(window).on('load',function(){
                  $('#profileUpdateSuccessModal').modal('show');
                });
             </script>";
  }

   ?>

</body>

</html>
