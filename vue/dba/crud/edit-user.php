<?php

require '../../../dao/database.php'; 
//require "../../utilities/PHPMailer-master/src/PHPMailer.php" ;
//use PHPMailer\PHPMailer\PHPMailer;

session_start ();

$account="";
$pass="";
$name= "";

if (isset($_SESSION['user']['profil'])) {
    $name= $_SESSION['user']['login'];
}
else {
    $name= $_SESSION['user']['prenom']." ".$_SESSION['user']['nom'];
}

if(isset($_POST['editlogin'])){
  if($_POST['editpwd1']==$_POST['editpwd2']){
     $id= $_GET['id'];
     $editlogin= $_POST['editlogin'];
     $editpwd= $_POST['editpwd1'];
     $editprofil= $_POST['editprofil'];
     Database::editAccount($id, $editlogin, $editpwd, $editprofil);
     
     /*date_default_timezone_set('Etc/UTC');

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug = 0;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "tls";
    $mail->Host = "smtp.gmail.com";
    $mail->Port = 25;
    $mail->Username = "dic2sm@gmail.com";
    $mail->Password = "passer2019";

    $mail->From = "admin@sm.org";
    $mail->FromName = "Admin SM";
    $mail->Subject = "SM - Account Update Message - New Password Setted";
    $mail->AltBody = "Hola, Here Your New Login Information \n - Login : $editlogin (it's your email) \n - Password : $editpwd .";
    $mail->MsgHTML("Hola, Here Your New Login Information \n - Login : $editlogin (it's your email) \n - Password : $editpwd .",dirname(__FILE__));
    //$mail->AddAttachment("files/files.zip");
    //$mail->AddAttachment("files/img03.jpg");
    $mail->AddAddress($editlogin, $editlogin);
    $mail->IsHTML(true);
    $mail->Send();*/

     header("Location: accounts.php?accountEdited=$id");
  }
  else{
    $pass= "unmatched";
    $id= $_GET['id'];
    $usert= $_GET['usertype'];
    header("Location: edit-user.php?id=$id&usertype=$usert&error=Error: unmatched pwd");
    //header("Refresh: 1;url=edit-user.php?id=".$_GET["id"]."&username=".$_GET['username']);
  }
}

if(isset($_POST['addlogin']) && isset($_POST['addpwd']) && isset($_POST['addprofil'])){
    Database::addAccount($_POST['addlogin'],$_POST['addpwd'],$_POST['addprofil']);
}

if(isset($_GET['id']) && isset($_GET['usertype'])){
  $account= Database::getOneUser($_GET['id'], $_GET['usertype']);
}



function users($data){
    ?>
					<?php 
					foreach($data as $val){
					?>
					<tr>
						<th scope="row"><?php echo $val['id'];?></th>
						<td><?php echo $val['login'];?></td>
                        <td><?php 
                          for($i=1; $i<=strlen($val['pwd']); $i++)  echo "#";?>
                        </td>
                        <td><?php echo $val['profil'];?></td>
            <td align="center">
							<a href="edit-user.php?id=<?= $val['id'];?>&usertype=account" class="text-primary"><i class="far fa-edit"></i></a> | 
							<a href="delete-user.php?id=<?= $val['id'];?>&usertype=account" class="text-danger"><i class="far fa-trash-alt"></i></a>
						</td>
					</tr>
						<?php } ?>
    <?php
}





?>



<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Site d'actualités</title>

  <!-- Custom fonts for this template -->
  <link href="../../../assets/utilities/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="../../../assets/utilities/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="../../../assets/utilities/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="..">
        <div class="sidebar-brand-icon rotate-n-15">
        <i class="far fa-hospital"></i>
        </div>
        <div class="sidebar-brand-text mx-3">PT <sup>Suivi Médical</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="..">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-user-md"></i>
          <span>Gestion des utilisateurs</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Liste des utilisateurs:</h6>
            <a class="collapse-item active" href="accounts.php">Accounts</a>
            <a class="collapse-item" href="editeurs.php">Médécins</a>
            <a class="collapse-item" href="admins.php">Adminis</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div class="input-group">
              <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>


            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $name; ?></span>
                <img class="img-profile rounded-circle" src="../../../assets/utilities/img/icon.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="../profile.php">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="../../../logout.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Gestion des utilisateurs</h1>
          <p class="mb-4">Opérations CRUD</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <div style="width: 100%;">
                  
                  <div style="width: 5%; float: left;">


                     <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="adduserDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <h3><i class="fas fa-user-plus"></i></h3>
              
              </a>
              <!-- Dropdown - Alerts -->
              <div class="dropdown-list dropdown-menu dropdown-menu-left shadow animated--fade-in" aria-labelledby="adduserDropdown" style="width: 600px">
                <h6 class="dropdown-header">
                  Add Account
                </h6>

                <form class="container" name="addu" action="" method="post">
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">Email | Login</label>
                        <input type="email" class="form-control" name="addlogin" placeholder="Email" required>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputPassword4">Password</label>
                        <input type="password" class="form-control" name="addpwd" placeholder="Password" required>
                      </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputState">Profil</label>
                        <select name="addprofil" class="form-control" required>
                          <option selected>Choose...</option>
                          <option value="medecin">Médécin</option>
                          <option value="infirmier">Infirmier</option>
                          <option value="secretaire">Sécrétaire</option>
                          <option value="admin">Admin</option>
                        </select>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-info">Add</button>
                </form>
              </div>
            </li>



                  
                  </div>  
                  <div style="width: 95%; margin-left: 500px;">
                      <h6 class="m-0 font-weight-bold text-primary">Liste des Comptes</h6>
                  </div>
              </div>

              








        








            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Login|Email</th>
                            <th>Password</th>
                            <th>Profil</th>
                            <th style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Login|Email</th>
                            <th>Password</th>
                            <th>Profil</th>
                            <th style="text-align: center;">Action</th>
                        </tr>
                    </tfoot>


                  <tbody>
                    <?php
                        users(Database::getAccounts());
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="../../../logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

    <!-- editUser Modal-->
    <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Account</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
        
        <form class="container" id="editUserForm" action="edit-user.php?id=<?= $_GET['id']?>&usertype=<?= $_GET['usertype']?>" method="post" role="form">
                    <div class="form-row">
                      <div class="form-group col-md-8">
                        <label for="inputEmail5">Email | Login</label>
                        <input type="email" class="form-control" name="editlogin" id="editlogin" value="<?= $account['login']?>" required>
                      </div>
                      <div class="form-group col-md-4">
                        <label for="inputPassword4">Password</label>
                        <input type="text" class="form-control" name="editpwd1" id="editpwd1" value="<?php if(isset($_GET['error'])) echo ""; else echo $account['pwd']?>" placeholder="<?php if(isset($_GET['error'])) echo $_GET['error'];?>" required>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-8" style="visibility: hidden;">
                       &nbsp;
                      </div>
                      <div class="form-group col-md-4 col-md-offset-8">
                        <input type="text" class="form-control" name="editpwd2" id="editpwd2" value="<?php if(isset($_GET['error'])) echo ""; else echo $account['pwd']?>" placeholder="<?php if(isset($_GET['error'])) echo $_GET['error'];?>"required>
                      </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="inputState">Profil</label>
                        <select id="editprofil" name="editprofil" class="form-control" required>
                          <option selected>Choose...</option>
                          <option value="editeur">Editeur</option>
                          <option value="admin">Admin</option>
                        </select>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button class="btn btn-secondary" type="button" data-dismiss="modal" onclick="window.location.href='accounts.php'">Cancel</button>
                      <input type="submit" name="editUserSubmit" id="editUserSubmit"class="btn btn-info" value="Edit">
                    </div>
                    
                </form>
        </div>
      </div>
    </div>
  </div>

 

  <!-- Bootstrap core JavaScript-->
  <script src="../../../assets/utilities/vendor/jquery/jquery.min.js"></script>
  <script src="../../../assets/utilities/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../../assets/utilities/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../../assets/utilities/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="../../../assets/utilities/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../../../assets/utilities/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="../../../assets/utilities/js/demo/datatables-demo.js"></script>

  <script type="text/javascript">
    $(window).on('load',function(){
        $('#editUserModal').modal('show');
    });
  </script>

  <script type="text/javascript">
  $('#editUserSubmit').click(function(){
    $('#editUserForm').submit();
  });
  </script>

  <script type="text/javascript">
    //if($('#editUserModal').hasClass('in')==true) window.location.href='accounts.php';
    $('#editUserModal').on('hidden.bs.modal', function () {
      window.location.href='accounts.php';
    });
  </script>


</body>

</html>
