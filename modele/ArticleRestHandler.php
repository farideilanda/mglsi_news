<?php
require_once("SimpleRest.php");
require_once("fonctions.php");
		
class ArticleRestHandler extends SimpleRest {

	function getAllArticles() {	

		$rawData= Modele::getList();


		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No Articles found!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];

		$this ->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = $this->encodeJson($rawData);
			echo $response;
		
			
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = $this->encodeXml($rawData);
			echo $response ;
			
		}
	}
	
	
	public function encodeJson($responseData) {
		$jsonResponse = json_encode($responseData);
		return $jsonResponse;		
	}
	
	public function encodeXml($responseData) {
		$xml = new SimpleXMLElement('<?xml version="1.0"?><articles></articles>');
		
		foreach($responseData as $art) {
			$article = $xml->addChild("article") ;
			$article->addChild("titre",$art->titre) ;
			$article->addChild("contenu",$art->contenu) ;
			$article->addChild("dateCreation",$art->dateCreation) ;
			$article->addChild("dateModification",$art->dateModification) ;
			$article->addChild("categorie",$art->categorie) ;
	
		}
		return $xml->asXML();
		
	
  
	}

	public function encodeGroupeXml($responseData) {
		$xml = new SimpleXMLElement('<?xml version="1.0"?><groupes></groupes>');
		
		foreach($responseData as $key=>$articles) {
			$xml->addChild("groupe",$key) ;

			$arts =	$xml->addChild("articles") ;
			foreach($articles as $art)
			{
			  
            $article = $arts->addChild("article") ;
			$article->addChild("titre",$art->titre) ;
			$article->addChild("contenu",$art->contenu) ;
			$article->addChild("dateCreation",$art->dateCreation) ;
			$article->addChild("dateModification",$art->dateModification) ;
			$article->addChild("categorie",$art->categorie) ;
			}
			
	
		}
		return $xml->asXML();
		
	
  
	}
	
	public function getCategorie($id) {

		
		$rawData = Modele::getItemByCategorie($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No mobiles found!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$this ->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = $this->encodeJson($rawData);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = $this->encodeHtml($rawData);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = $this->encodeXml($rawData);
			echo $response;
		}
	
}

	

	public function getGroupe() {

		
		$rawData = Modele::getRegroupeCategorie() ;

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No Articles found!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$this ->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = $this->encodeJson($rawData);
			echo $response;
		
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = $this->encodeGroupeXml($rawData);
			echo $response;
		}
	}
}
?>