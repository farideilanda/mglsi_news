CREATE TABLE `account` (
  `id` int NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(255) NOT NULL,
  `pwd` VARCHAR(255) NOT NULL,
  `profil` VARCHAR(255) NOT NULL,
  PRIMARY KEY (id,login)
);

CREATE TABLE `admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `numAdmin` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  CONSTRAINT pk_adm PRIMARY KEY (id,email),
  CONSTRAINT fk_acc_adm FOREIGN KEY (email) REFERENCES account(login)
); 

CREATE TABLE `editeur` (
  `id` int NOT NULL AUTO_INCREMENT,
  `numEditeur` varchar(25) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  CONSTRAINT pk_edo PRIMARY KEY (id,email),
  CONSTRAINT fk_acc_edi FOREIGN KEY (email) REFERENCES account(login)
);



INSERT INTO `account` (`login`, `pwd`, `profil`) VALUES ('thisiswhywecode@code.org', 'passer', 'admin');

INSERT INTO `account` (`login`, `pwd`, `profil`) VALUES ('faride.ilanda@ucad.edu.sn', 'passer', 'editeur');
INSERT INTO `account` (`login`, `pwd`, `profil`) VALUES ('pape.ngom@ucad.edu.sn', 'passer', 'editeur');


INSERT INTO `editeur` (`numEditeur`, `nom`, `prenom`, `email`) VALUES ('e1', 'Pape', 'Ngom', 'pape.ngom@ucad.edu.sn');
INSERT INTO `editeur` (`numEditeur`,`nom`, `prenom`, `email`) VALUES ('e2', 'Ilanda', 'Faride', 'faride.ilanda@ucad.edu.sn');

INSERT INTO `admin` (`numAdmin`, `nom`, `prenom`, `email`) VALUES ('007', 'Ilanda', 'Faride', 'thisiswhywecode@code.org');


