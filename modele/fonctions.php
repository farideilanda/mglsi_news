
<?php

require_once "dao/Acces.php" ;
 class Modele{
	private static $_bdd  ;

	

	static function  getList( $table='Article')
	{
		return Dao::listerElements($table)		;
	}

	static function getItem( $id, $table='Article')
	{
		return Dao::recupererElement($id) ;
	}

	static function getItemByCategorie( $catId)
	{
		return Dao::listerElementByCat($catId) ;
	}

	static function getRegroupeCategorie( )
	{
		$liste = array( 
			"Sport" => self::getItemByCategorie(1),
			"Sante" => self::getItemByCategorie(2),
			"Education" => self::getItemByCategorie(3),
			"Politique" => self::getItemByCategorie(4)
			 );
		return $liste ;
	}

	




}






	
?>