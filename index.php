<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Actualités MGLSI</title>
	<link rel="stylesheet" type="text/css" href="assets/design/style.css">

	<link href="assets/utilities/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<link href="assets/utilities/css/sb-admin-2.min.css" rel="stylesheet">

	<link href="assets/utilities/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>
<body>
<?php 
	    require_once "controlleur/controlleur.php" ;
	     require_once "modele/fonctions.php" ;

          $controlleur1 = new Controlleur() ;
          if(!isset($_GET['action']))
          	 $controlleur1->afficherPageAccueil() ;
        else
            	if ($_GET['action']=="article")
		{
			$id= (int) $_GET['id'] ;
			$controlleur1->afficherArticle($id) ;
		}
		else if  ($_GET['action']=="categorie")
		{
			$idCat= (int) $_GET['idCat'];
			$controlleur1->afficherArticleCategorie($idCat) ;
			
		}
		
		?>
	</div>
	      
</body>
</html>

<script src="assets/utilities/vendor/jquery/jquery.min.js"></script>
  <script src="assets/utilities/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="assets/utilities/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="assets/utilities/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="assets/utilities/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="assets/utilities/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="assets/utilities/js/demo/datatables-demo.js"></script>