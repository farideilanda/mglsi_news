<?php
use League\Uri\Schemes\Data;

class Database{ 
    private static $dbName = 'mglsi_news';
    private static $dbHost = 'localhost';
    private static $dbUsername = 'root';
    private static $dbUserPassword = '';
    private static $cont = null;

    public function __construct(){
        die('Init function is not allowed');
    } 
    public static function connect(){
        if ( null == self::$cont ){
            try{
                self::$cont = new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, 
                                        self::$dbUsername, 
                                        self::$dbUserPassword,
                                        array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
                );
            }catch(PDOException $e){
                die($e->getMessage()); 
            }
        }
        return self::$cont;
    }
 
    public static function disconnect(){
        self::$cont = null;
    }

    public static function getEditeurs(){
        $pdo = Database::connect();
        $sql = "SELECT * FROM editeur";
        $q = $pdo->prepare($sql);
        $q->execute();
        $data = $q->fetchAll();
        Database::disconnect();
        return $data;
    } 

    public static function getAdmins(){
        $pdo = Database::connect();
        $sql = "SELECT * FROM admin";
        $q = $pdo->prepare($sql);
        $q->execute();
        $data = $q->fetchAll();
        Database::disconnect();
        return $data;
    }

    public static function getAccounts(){
        $pdo = Database::connect();
        $sql = "SELECT * FROM account ORDER BY id DESC"; // WHERE profil NOT IN (?)
        $q = $pdo->prepare($sql);
        $q->execute();
        $data = $q->fetchAll();
        Database::disconnect();
        return $data;
    }

    public static function getArticles(){
        $pdo = Database::connect();
        $sql = "SELECT * FROM article"; // WHERE profil NOT IN (?)
        $q = $pdo->prepare($sql);
        $q->execute();
        $data = $q->fetchAll();
        Database::disconnect();
        return $data;
    }

    public static function existAccount($login){
        $pdo = Database::connect();
        $data= null;

        $sql = "SELECT * FROM account WHERE login= ?";
        $q= $pdo->prepare($sql);
        $q->execute(array($login));
        $data = $q->fetch();
    
        Database::disconnect();
        if(empty($data))    return false;
        else    return true;
    }

    public static function checkAccount($login, $pwd, $profil){
        $pdo = Database::connect();
        $data= null;

        $sql = "SELECT * FROM account WHERE login= ? AND pwd= ? AND profil= ?";
        $q= $pdo->prepare($sql);
        $q->execute(array($login, $pwd, $profil));
        $data = $q->fetch();
    
        Database::disconnect();
        return $data;
    }

    public static function checkAdmin($login, $pwd){
        $pdo = Database::connect();
        $data= null;
        $profil= "admin";
        $account= "";

        if(empty($account=Database::checkAccount($login, $pwd, $profil))) return null;
        else {
            $sql = "SELECT * FROM admin WHERE email= ?";
            $q= $pdo->prepare($sql);
            $q->execute(array($account['login']));
            $data = $q->fetch();
        }
        Database::disconnect();
        if(empty($data)) return $account;
        else    return $data;
    }

    public static function checkEditeur($login, $pwd){
        $pdo = Database::connect();
        $data= null;
        $profil= "editeur";
        $account= "";
        
        if(empty($account=Database::checkAccount($login, $pwd, $profil))) return null;
        else {
            $sql = "SELECT * FROM editeur WHERE email= ?";
            $q= $pdo->prepare($sql);
            $q->execute(array($account['login']));
            $data = $q->fetch();
        }
        Database::disconnect();
        if(empty($data)) return $account;
        else    return $data;
    }

    public static function addAccount($login, $pwd, $profil){
        $pdo = Database::connect();

        $sql = "INSERT INTO account(login,pwd,profil) values (?,?,?)";
        $q= $pdo->prepare($sql);
        $q->execute(array($login, $pwd, $profil));
        
        Database::disconnect();
    }
    

    public static function editAccount($id, $login, $pwd, $profil){
        $pdo = Database::connect();

        $sql = "UPDATE account SET login= ?, pwd= ?, profil= ? WHERE id= ?";
        $q= $pdo->prepare($sql);
        $q->execute(array($login, $pwd, $profil, $id));
        
        Database::disconnect();
    }

    public static function deleteAccount($id, $login){
        $pdo = Database::connect();

        $sql = "DELETE FROM account WHERE id= ? AND login= ?";
        $q= $pdo->prepare($sql);
        $q->execute(array($id, $login));
        
        Database::disconnect();
    }

    public static function deleteEditeur($id, $email){
        $pdo = Database::connect();

        $sql = "DELETE FROM editeur WHERE id= ? AND email= ?";
        $q= $pdo->prepare($sql);
        $q->execute(array($id,$email));

        Database::disconnect();
    }

    public static function getOneUser($id, $user){
        $pdo = Database::connect();
        $data= null;

        switch ($user) {
            case 'account':
                $sql = "SELECT * FROM account WHERE id= ?";
                $q= $pdo->prepare($sql);
                $q->execute(array($id));
                $data = $q->fetch();
                break;
            case 'editeur':
                $sql = "SELECT * FROM editeur WHERE id= ?";
                $q= $pdo->prepare($sql);
                $q->execute(array($id));
                $data = $q->fetch();
                break;
            case 'admin':
                $sql = "SELECT * FROM admin WHERE id= ?";
                $q= $pdo->prepare($sql);
                $q->execute(array($id));
                $data = $q->fetch();
                break;
        }
        
        
        Database::disconnect();
        return $data;
    }

    public static function getOneAccount($login){
        $pdo = Database::connect();
        $sql = "SELECT * FROM account WHERE login= ?";
        $q= $pdo->prepare($sql);
        $q->execute(array($login));
        $data = $q->fetch();
        
        Database::disconnect();
        return $data;
    }

    public static function getOneArticle($id){
        $pdo = Database::connect();
        $sql = "SELECT id, titre, contenu, DATE_FORMAT(dateCreation, '%Y-%m-%dT%H:%i') AS dateCreation, DATE_FORMAT(dateModification, '%Y-%m-%dT%H:%i') AS dateModification, categorie FROM article WHERE id= ?";
        $q= $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch();
        
        Database::disconnect();
        return $data;
    }

    public static function updatePassword($login, $pwd){
        $pdo = Database::connect();

        $sql = "UPDATE account SET pwd= ? WHERE login= ?";
        $q= $pdo->prepare($sql);
        $q->execute(array($pwd, $login));

        Database::disconnect();
    }

    public static function updateAdmin($id, $numAdmin, $nom, $prenom, $email){
        $pdo = Database::connect();

            $sql = "UPDATE admin SET numAdmin= ?, nom= ?, prenom= ?, email= ? WHERE id= ? AND email= ?";
                $q= $pdo->prepare($sql);
                $q->execute(array($numAdmin, $nom, $prenom,$email, $id, $email));

        Database::disconnect();
    }

    public static function updateEditeur($id,$numEditeur,$nom,$prenom,$email){
        $pdo = Database::connect();

                $sql = "UPDATE editeur SET numEditeur= ?, nom= ?, prenom= ?, email= ? WHERE id= ? AND email= ?";
                $q= $pdo->prepare($sql);
                $q->execute(array($numEditeur,$nom,$prenom,$email,$id,$email));

        Database::disconnect();
    }


    public static function createAdmin($numAdmin, $nom, $prenom, $email){
        $pdo = Database::connect();

        $sql = "INSERT INTO admin(numAdmin,nom,prenom,email) values (?,?,?,?)";
        $q= $pdo->prepare($sql);
        $q->execute(array($numAdmin, $nom, $prenom, $email));
        
        Database::disconnect();
    }

    public static function createEditeur($numEditeur,$nom,$prenom,$email){
        $pdo = Database::connect();

        $sql = "INSERT INTO editeur(numEditeur,nom,prenom,email) values (?,?,?,?)";
        $q= $pdo->prepare($sql);
        $q->execute(array($numEditeur,$nom,$prenom,$email));
        
        Database::disconnect();
    }

    public static function addArticle($titre,$contenu,$categorie){
        $pdo = Database::connect();
        $dc= new DateTime();
        $dm= new DateTime();

        $sql = "INSERT INTO article(titre,contenu,dateCreation,dateModification,categorie) values (?,?, now(), now(), ?)";
        $q= $pdo->prepare($sql);
        $q->execute(array($titre, $contenu, $categorie));
        
        Database::disconnect();
    }

    public static function editArticle($id, $titre, $contenu, $categorie){
        $pdo = Database::connect();

            $sql = "UPDATE article SET titre= ?, contenu= ?, dateModification= now(), categorie= ? WHERE id= ?";
                $q= $pdo->prepare($sql);
                $q->execute(array($titre, $contenu, $categorie, $id));

        Database::disconnect();
    }

    public static function deleteArticle($id){
        $pdo = Database::connect();

        $sql = "DELETE FROM article WHERE id= ?";
        $q= $pdo->prepare($sql);
        $q->execute(array($id));

        Database::disconnect();
    }




    
}


?>