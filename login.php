<?php  
    require 'dao/database.php';


    if($_SERVER["REQUEST_METHOD"]== "POST" && !empty($_POST)){ 
            //on initialise nos messages d'erreurs;
            $usernameError =''; 
            $pwdError='';  
            // on recupère nos valeurs
            $username=htmlentities(trim($_POST['username']));
            $pwd=htmlentities(trim($_POST['password']));

            // on vérifie nos champs 
            $valid = true;
            
            if(empty($username)){ 
                $nomError = 'Login invalide'; 
                $valid = false;
            }
            if(empty($pwd)){ 
                $nomError = 'Mot de passe invalide'; 
                $valid = false;
            }
            
            // si les données sont présentes et bonnes, on se connecte à la base 
            if ($valid) { 
                
                if(!empty($data1= Database::checkAdmin($username, $pwd))){
                    session_start ();
                    $_SESSION['user']= $data1;
                    header("Location: vue/dba/");
                }
                else{
                    if(!empty($data2= Database::checkEditeur($username, $pwd))){
                        session_start ();
                        $_SESSION['user']= $data2;
                        header("Location: vue/editeur/");
                    }
                    else{
                        header("Location: login.php?error=1");
                    }
                }
            }
    }
?>






<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PT</title>

  <!-- Custom fonts for this template-->
  <link href="assets/utilities/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="assets/utilities/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                  </div>
                  <form class="user" action="" method="post">
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" name="username" aria-describedby="emailHelp" placeholder="Enter Email Address..." required>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" name="password" placeholder="Password" required>
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block" name="valider">
                      Login
                    </button>
                    <hr>
                    <a href="login.php" class="btn btn-google btn-user btn-block">
                      <i class="fab fa-google fa-fw"></i> Login with Google
                    </a>
                    <a href="login.php" class="btn btn-facebook btn-user btn-block">
                      <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                    </a>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="forgot-password.php">Forgot Password?</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

    <!-- accountDeleteSuccessModal Modal-->
    <div class="modal fade" id="loginErrorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Loggin In</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body"><strong>Login</strong> or <strong>Password</strong> incorrrect !</div>
        <div class="modal-footer">
          <a class="btn btn-danger" href="index.php">Retry again</a>
        </div>
      </div>
    </div>
  </div>

        <!-- accountDeleteSuccessModal Modal-->
  <div class="modal fade" id="mailSentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Mail Sending</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">A <strong>mail</strong> with <strong>reseted password</strong> has been set to you !</div>
        <div class="modal-footer">
          <a class="btn btn-danger" href="login.php">Done</a>
        </div>
      </div>
    </div>
  </div>

         <!-- accountDeleteSuccessModal Modal-->
         <div class="modal fade" id="unavailableAccountModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Checking Email availability</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Error your mail <strong><?=$_GET['email']?></strong> <strong> does not exist</strong> !</div>
        <div class="modal-footer">
          <a class="btn btn-danger" href="index.php">Done</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="assets/utilities/vendor/jquery/jquery.min.js"></script>
  <script src="assets/utilities/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="assets/utilities/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="assets/utilities/js/sb-admin-2.min.js"></script>

  <?php
  if(isset($_GET['error']) && $_GET['error']==1){
        echo "<script type='text/javascript'>
                $(window).on('load',function(){
                  $('#loginErrorModal').modal('show');
                });
             </script>";
  }

  if(isset($_GET['sentMail']) && $_GET['sentMail']==1){
    echo "<script type='text/javascript'>
            $(window).on('load',function(){
              $('#mailSentModal').modal('show');
            });
         </script>";
   }

   if(isset($_GET['availableAccount']) && $_GET['availableAccount']==0){
    echo "<script type='text/javascript'>
            $(window).on('load',function(){
              $('#unavailableAccountModal').modal('show');
            });
         </script>";
   }

   ?>

</body>

</html>
