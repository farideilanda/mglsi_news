$(document).ready(function(){	
	$("#editUserForm").submit(function(event){
		submitForm();
		return false;
	});
});

function submitForm(){
    $.ajax({
       type: "POST",
       url: "edit-user.php",
       cache:false,
       data: $('form#editUserForm').serialize(),
       success: function(response){
           $("#editUserModal").modal('hide');
       },
       error: function(){
           alert("Error");
       }
   });
}