CREATE TABLE `account` (
  `id` int NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(255) NOT NULL,
  `pwd` VARCHAR(255) NOT NULL,
  `profil` VARCHAR(255) NOT NULL,
  PRIMARY KEY (id,login)
);

CREATE TABLE `service` (
  `id` int NOT NULL AUTO_INCREMENT,
  `codeService` VARCHAR(255) NOT NULL,
  `designation` VARCHAR(255) NOT NULL,
  `chefService` VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);


CREATE TABLE `admin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `numAdmin` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `adresse` varchar (255) NOT NULL,
  CONSTRAINT pk_adm PRIMARY KEY (id,email),
  CONSTRAINT fk_acc_adm FOREIGN KEY (email) REFERENCES account(login)
); 

CREATE TABLE `medecin` (
  `id` int NOT NULL AUTO_INCREMENT,
  `numMedecin` varchar(25) NOT NULL,
  `specialite` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `adresse` varchar (255) NOT NULL,
  `service` varchar (255) NOT NULL,
  CONSTRAINT pk_med PRIMARY KEY (id,email),
  CONSTRAINT fk_acc_med FOREIGN KEY (email) REFERENCES account(login)
);

CREATE TABLE `infirmier` (
  `id` int NOT NULL AUTO_INCREMENT,
  `numInfirmier` varchar(25) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `adresse` varchar (255) NOT NULL,
  `service` varchar (255) NOT NULL,
  CONSTRAINT pk_inf PRIMARY KEY (id,email),
  CONSTRAINT fk_acc_inf FOREIGN KEY (email) REFERENCES account(login)
);

CREATE TABLE `secretaire` (
  `id` int NOT NULL AUTO_INCREMENT,
  `numSecretaire` varchar(25) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `adresse` varchar (255) NOT NULL,
  `service` varchar (255) NOT NULL,
  CONSTRAINT pk_sec PRIMARY KEY (id,tel),
  CONSTRAINT fk_acc_sec FOREIGN KEY (email) REFERENCES account(login)
);

CREATE TABLE `message` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `auteur` VARCHAR(255) NOT NULL,
  `destinataire` VARCHAR(255) NOT NULL,
  `contenu` VARCHAR(255) NOT NULL,
  `statut` int NOT NULL,
  PRIMARY KEY (id)
);


INSERT INTO `message` (`date`, `auteur`,`destinataire`,`contenu`, `statut`) VALUES ('2019-07-28', 'faride.ilanda@ucad.edu.sn','djeddah.beyatt@ucad.edu.sn', 'Bonsoir redige une nouvelle fiche de temperature pour le patient X.', 1);
INSERT INTO `message` (`date`, `auteur`,`destinataire`,`contenu`, `statut`) VALUES ('2019-07-28', 'pape.ngom@ucad.edu.sn','djeddah.beyatt@ucad.edu.sn', 'Ok d accord.', 1);

INSERT INTO `service` (`codeService`, `designation`, `chefService`) VALUES ('s1', 'Chirurgie', 'faride.ilanda@ucad.edu.sn');
INSERT INTO `service` (`codeService`, `designation`, `chefService`) VALUES ('s2', 'Pharmacie','djeddah.beyatt@ucad.edu.sn');

INSERT INTO `account` (`login`, `pwd`, `profil`) VALUES ('thisiswhywecode@code.org', 'passer', 'admin');

INSERT INTO `account` (`login`, `pwd`, `profil`) VALUES ('faride.ilanda@ucad.edu.sn', 'passer', 'medecin');
INSERT INTO `account` (`login`, `pwd`, `profil`) VALUES ('djeddah.beyatt@ucad.edu.sn', 'passer', 'medecin');

INSERT INTO `account` (`login`, `pwd`, `profil`) VALUES ('julia.diagne@ucad.edu.sn', 'passer', 'infirmier');

INSERT INTO `account` (`login`, `pwd`, `profil`) VALUES ('pape.ngom@ucad.edu.sn', 'passer', 'secretaire');

INSERT INTO `medecin` (`numMedecin`, `specialite`, `nom`, `prenom`, `tel`, `email`, `adresse`, `service`) VALUES ('A1', 'Chirurgie', 'Beyatt', 'Djeddah', '778954512', 'djeddah.beyatt@ucad.edu.sn', 'Gueule Tapée', 'Chirurgie');
INSERT INTO `medecin` (`numMedecin`, `specialite`, `nom`, `prenom`, `tel`, `email`, `adresse`, `service`) VALUES ('A2', 'Oto-rhino-laryngologie.', 'Ilanda', 'Faride', '784303405', 'faride.ilanda@ucad.edu.sn', 'Mermoz', 'Pharmacie');

INSERT INTO `infirmier` (`numInfirmier`, `nom`, `prenom`, `tel`, `email`, `adresse`, `service`) VALUES ('AA1', 'Diagne', 'Julia', '771234578', 'julia.diagne@ucad.edu.sn', 'St Louis', 'Chirurgie');

INSERT INTO `secretaire` (`numSecretaire`, `nom`, `prenom`, `tel`, `email`, `adresse`, `service`) VALUES ('AAA1', 'Ngom', 'Pape Ousseynou', '771531102', 'pape.ngom@ucad.edu.sn', 'Castors', 'Chirurgie');

INSERT INTO `admin` (`numAdmin`, `nom`, `prenom`, `tel`, `email`, `adresse`) VALUES ('007', 'Ilanda', 'Faride', '784303405', 'thisiswhywecode@code.org', 'Mermoz - Comico');
